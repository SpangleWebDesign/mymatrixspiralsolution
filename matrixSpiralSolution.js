const n = 3;

var oArr = [];

//Fill Array with n-times empty arrays
for(var i = 0; i < n; i++){
	oArr.push([]);
}


var counter = 1;
var startRow = 0;
var endRow = n-1;
var startColumn = 0;
var endColumn = n-1;


while(startRow <= endRow && startColumn <= endColumn){
	
	//top row
	for(var i = startColumn; i <= endColumn; i++){
		oArr[startColumn][i] = counter;
		counter++;
	}
	startRow++;

	//right Side

	for(var i = startRow; i <= endRow; i++){
		oArr[i][endColumn] = counter;
		counter++;
	}
	endColumn --;

	//Bottom Row

	for(var i = endColumn; i >= startColumn; i--){
		oArr[endRow][i] = counter;
		counter++;
	}
	endRow--;
	
	//Left Side

	for(var i = endRow; i >= startRow; i--){
		oArr[i][startColumn] = counter;
		counter++;
	}
	startColumn++;
	//Will now loop back through the inner rows and columns.	
}

//log results
for(var i = 0; i < n; i++){
	console.log(oArr[i]);
}